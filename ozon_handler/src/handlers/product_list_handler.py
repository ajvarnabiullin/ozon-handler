from .base import BaseHandler
from sqlalchemy.ext.declarative import declarative_base
from typing import TypeVar, Type
from ..models import LSOrder, ItemModel, OrderItemsModel, WarehouseModel
TBaseModel = TypeVar('TBaseModel', bound=declarative_base())

class ProductListHandler(BaseHandler):
    """Обработчик для сохранения списка продуктов"""
    
    def avg_commission_and_logistics(self, entity: TBaseModel) -> TBaseModel:
        item = self.base_session.query(ItemModel).filter(
            ItemModel.article_number == entity.article_number
        ).first()
        query = self.base_session.query(
            OrderItemsModel, ItemModel
        ).filter(
            OrderItemsModel.status == 'Продажа'
        ).filter(
            OrderItemsModel.item == item
        ).order_by(
            OrderItemsModel.created_at.desc()
        ).limit(10).all()
        
        amount_comission = 0
        amount_logistics = 0
        print(len(query))
        for order_item in query:
            amount_comission += order_item.comission
            amount_logistics += order_item.logistics
            
        entity.avg_comission = amount_comission/len(query) if len(query) else 0
        entity.avg_logistics = amount_logistics/len(query) if len(query) else 0
        
        return entity
        
        
        
        
            
    
    def items_module(self, product_list) -> TBaseModel:
        for products in product_list:
            for product in products.items:
                entity = self.base_model(
                    brand='-',
                    size='-',
                    nmid='-',
                    cost_price='-'
                )
                product_id = product.product_id
                offer_id = product.offer_id
                print(product_id)
                print(offer_id)
                product_type = self.get_model_object('ProductInfoModel')
                item = self.session.query(product_type).filter(
                    product_type.id==product_id).filter(
                    product_type.offer_id==offer_id
                ).order_by(product_type.created_at.desc()).first()
                
                entity.name = item.name
                
                entity.article_number = item.offer_id
                entity.sale_price = item.price
                entity.image = item.primary_image.replace('media-e/', 'media-e/wc200/')
                for source in item.sources:
                    if source.source == 'fbs':
                        entity.sku_fbs = source.sku
                    if source.source == 'fbo':
                        entity.sku_fbo = source.sku
                entity.category = ''
                
                entity = self.avg_commission_and_logistics(entity=entity)
                query = self.base_session.query(type(entity)).filter(
                    ItemModel.article_number == entity.article_number
                    ).all()
                
                if len(query) == 0:
                    entity = self.save(entity=entity)
                else:
                    
                    entity = self.update(entity=entity)
        return True
        
    def     start(self):
        """Запуск обработчика"""
        
        product_list_type = self.get_model_object('ProductListModel')
        product_list = self.session.query(product_list_type).filter(product_list_type.seller_id == self.seller.pk).all()
        entities_save = self.items_module(product_list=product_list)
        
        return True
        
        
            