from ozon_data_collector.src.models import ProductListModel
from pydoc import locate
from sqlalchemy.ext.declarative import declarative_base
from typing import TypeVar, Type
from ozon_db.core.connection import DBContext
from ...models import ItemModel
from ozon_db.orm.base import BaseModel
from sqlalchemy import inspect

TBaseModel = TypeVar('TBaseModel', bound=declarative_base())

class BaseHandler:
    """Базовый класс обработчика"""
    
    def __init__(self, base_model_name: str='', client_id: str='', api_key: str=''):
        self.db_context = DBContext(db_name='ozon_processed', BaseModel=BaseModel)
        self.session = DBContext(db_name='ozon_db5')._session
        self.base_model = ItemModel
        self.seller = self.get_model_object('Seller')
        self.seller = self.session.query(self.seller).filter(
            self.seller.client_id == client_id
        ).filter(api_key==api_key).first()
        self.base_session = self.db_context._session
        
    
    def get_model_object(self, model_name: str='') -> TBaseModel:
        model = locate('ozon_data_collector.src.models.{}'.format(model_name))
        print(model)
        print(model.__dict__)
        if model is None:
            raise ImportError("Could not import view type %r" % model_name)
        return model
    
    def get_entity_field_value(self, entity: TBaseModel, entity_field: str=''):
        """Возвращает значение запрошенной переменной из нужного объекта модели"""
        
        return getattr(entity, entity_field, None)
    
    def save(self, entity: TBaseModel) -> TBaseModel:
        with self.db_context.session() as s:
            s.add(entity)
            s.commit()
            s.refresh(entity)
        return entity
    
    def object_as_dict(self, obj):
        return {c.key: getattr(obj, c.key)
            for c in inspect(obj).mapper.column_attrs}
        
    def update(self, entity: TBaseModel) -> TBaseModel:
        with self.db_context.session() as s:
            s.query(type(entity)).where(type(entity).pk == entity.pk).update(self.object_as_dict(entity))
            s.commit()
        return entity
    
        