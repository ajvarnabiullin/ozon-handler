from ozon_db.orm.base import DeclarativeBase
from sqlalchemy import DateTime, Integer, Column
from datetime import datetime
from sqlalchemy.ext.declarative import declared_attr

class Base(object):
    @declared_attr
    def __tablename__(cls):
        return cls.__name__.lower()
    # __table_args__ = {'psql_engine': 'PostgreSQL'}

    pk = Column(Integer, primary_key=True)
    created_at = Column(DateTime, default=datetime.utcnow())
    updated_at = Column(DateTime, onupdate=datetime.utcnow())
    
    
from sqlalchemy.ext.declarative import declarative_base

BaseModel = declarative_base(cls=Base)

