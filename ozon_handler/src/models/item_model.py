from .base import BaseModel
from sqlalchemy import Column, String, Integer, ForeignKey, DateTime, Float
from sqlalchemy.orm import relationship

class ItemModel(BaseModel):
    """Модель товара"""
    __tablename__ = 'item'
    
    name = Column(String, default='-')
    category = Column(String, default='-')
    brand = Column(String, default='-')
    nmid = Column(String, default='-')
    article_number = Column(String, default='-')
    sale_price = Column(Float, default=0)
    size = Column(String, default='-')
    image = Column(String, default='-')
    cost_price = Column(String, default=0)
    avg_comission = Column(Float, default=0)
    avg_logistics = Column(Float, default=0)
    sku_fbo = Column(String, default='-')
    sku_fbs = Column(String, default='-')
    
    warehouse_id = Column(Integer, ForeignKey('warehouse.pk'))
    
    order_items_id = Column(Integer, ForeignKey('order_item_model.pk'))
