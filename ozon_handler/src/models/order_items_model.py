from .base import BaseModel
from sqlalchemy import Column, String, Integer, ForeignKey, DateTime, Float
from sqlalchemy.orm import relationship

class OrderItemsModel(BaseModel):
    """Товар из заказа"""
    __tablename__ = 'order_item_model'
    
    ls_order_id = Column(Integer, ForeignKey('ls_order.pk'))
    # ls_order = relationship("LSOrder", back_populates="order_items", cascade="all, delete-orphan", uselist=False)
    
    item = relationship("ItemModel", cascade="all, delete-orphan", uselist=False)
    
    price = Column(Float, default=0)
    listed_before_deductions = Column(Float, default=0)
    comission = Column(Float, default=0)
    quantity = Column(Integer, default=0)
    logistics = Column(Float, default=0)
    sale_or_return_date = Column(DateTime)
    status = Column(String, default='-')
    cost_price = Column(Float, default=0)
    
    