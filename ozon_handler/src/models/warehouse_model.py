from .base import BaseModel
from sqlalchemy import Column, String, Integer, ForeignKey, DateTime
from sqlalchemy.orm import relationship

class WarehouseModel(BaseModel):
    """Модель для складов"""
    __tablename__ = 'warehouse'
    
    name = Column(String, default='-')
    item = relationship('ItemModel', cascade="all, delete-orphan")
    size = Column(String, default='-')
    item_count = Column(Integer, default=0)
    from_client = Column(Integer, default=0)
    to_client = Column(Integer, default=0)
    beetween_warehouse = Column(Integer, default=0)
    in_delivery = Column(Integer, default=0)
    type = Column(String, default='-')