from .base import BaseModel
from sqlalchemy import Column, String, Integer, ForeignKey, DateTime
from sqlalchemy.orm import relationship

class LSOrder(BaseModel):
    """Модель для хранения обработанных заказов"""
    __tablename__ = 'ls_order'
    
    order_id = Column(Integer, default=0)
    order_number = Column(Integer, default=-1)
    order_date = Column(DateTime)
    type = Column(String, default='-')
    region = Column(String, default='-')
    
    order_items = relationship('OrderItemsModel', cascade="all, delete-orphan")
    